# Projet DJV 2020 - MAB Productions

![](MAB.png)

## Info

Unity version : 2019.4.14f1

## Présentation de notre sujet 

https://docs.google.com/document/d/1v8DjLAxh-c85CO4-5axUeI3dLyHLzV37hs8Lk1vqO3Y/edit#

## Diaporamas de présentation

https://docs.google.com/presentation/d/1SCLtoUTesS4uKOC8-Q3ZaVmggc-kSKaqHy-7WyfP79E/edit#slide=id.p

## Changements depuis la présentation

> Quelques éléments on été modifiés depuis la présentation :
> * Correction d'une faute dans l'UI de jeu au niveau du score
> * Modification de la photo du mage dans la scène de sélection de personnage
> * Ajout dans la scène de sélection de personnage des informations sur chacun (nombre de parties effectuées, vague maximale, score maximal)
> * En découle des modifications dans les scripts GameBehaviour.cs et PlayerSelectionManager.cs
> * Modification du comportement des projectiles au-dessus des cases de trou : décochage d'une case dans l'éditeur Unity afin de ne pas détruire les flèches et boules de feu au-dessus de la lave
> * Ajustement du fonctionnement des Traps de l'archer
> * Ajout au projet du script MoveTo.cs permettant de régir le déplacement des ennemis
> * Tentative d'ajout d'un NavMesh et d'un NavMeshAgent aux ennemis


***Note liée au fonctionnement du jeu :***  
* Commandes du personnage : Z : haut | Q : gauche | S : bas | D : droite | Souris : visée
* Actions du personnage : Action principale : Clic Gauche Souris | Action secondaire : Clic Droit Souris | Action tertiaire : Touche A ou ESPACE
* Lorsque dans la scène de sélection de joueur est marqué sous la photo d'un personnage "Vague Record : 0", cela signifie qu'il n'a jamais été joué

***Note liée au développement du jeu :***  
Nous avons laissé les scènes de développement du jeu, qui permettaient de tester les fonctionnalités indépendamment les unes des autres. Seules les scènes de jeu sont utilisées lors du build.