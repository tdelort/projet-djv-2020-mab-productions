﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameBehaviour : MonoBehaviour
{

    //UI part
    [SerializeField]
    public Text scoreText;
    [SerializeField]
    public Text timeText;
    public bool startCounting = false;
    private float timeSpent = 0f;

    [SerializeField]
    public Text waveText;

    [SerializeField]
    public Image coolDown1;
    [SerializeField]
    public Image coolDown2;

    [SerializeField]
    public Slider lifeSlider;
    [SerializeField]
    private RectTransform fillRect;
    private float targetValue = 0f;
    private float curValue = 0f;

    public int score = 0;
    public int best_score;
    //Game part
    [SerializeField]
    private ArenaMaker arenaMaker;
    private int nbDeath = 0;
    private bool allDead = false;
    private int waveNumber=1;
    private PlayerType playerSelected;
    public static int[] fiboValues = new int[] {0,1,1,2,3,5,8,13,21,34,53};

    [SerializeField]
    EnemyFactory enemyFactory;

    [SerializeField]
    GameObject knight;
    [SerializeField]
    GameObject archer;
    [SerializeField]
    GameObject mage;
    [SerializeField]
    CameraMovement cam;

    [SerializeField]
    GameObject popup;
    [SerializeField]
    Text popupText;
    [SerializeField]
    Button popupButton;

    private List<Vector3> spawnsList;
    private bool isPaused;

    public void Start()
    {
        playerSelected = GameManager.instance.getPlayerType();
        ActivatePlayer(playerSelected);
        spawnsList = arenaMaker.GetSpawns();
        SetWaveOnScreen(waveNumber);
        StartCoroutine(Wave(waveNumber));
        allDead = false;
        popupButton.onClick.AddListener(() => SceneManager.LoadScene(0));
        best_score=PlayerPrefs.GetInt("BestScore"+playerSelected.ToString(),0);
    }

    public void ActivatePlayer(PlayerType playerSelected)
    {
        switch(playerSelected){
            case PlayerType.KNIGHT:
                Debug.Log("Playing KNIGHT");
                knight.SetActive(true);
                cam.SetTarget(knight.transform);
                SetLifeSliderMaxValue(50);
                break;
            case PlayerType.MAGE:
                Debug.Log("Playing MAGE");
                mage.SetActive(true);
                cam.SetTarget(mage.transform);
                SetLifeSliderMaxValue(40);
                break;
            case PlayerType.BOWMAN:
                Debug.Log("Playing BOWMAN");
                archer.SetActive(true);
                cam.SetTarget(archer.transform);
                SetLifeSliderMaxValue(30);
                break;
            default:
                Debug.Log("Playing KNIGHT");
                knight.SetActive(true);
                break;
        }
    }

    public void Update()
    {
        timeSpent += Time.deltaTime;
        timeText.text = "Temps de jeu : " + timeSpent.ToString("f2");

        if(allDead)
        {   
            if(waveNumber >= 10)
            {
                //WIN
                Win();
            }
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlayableCharacter>().ResetPos();
            Debug.Log("nu wave");
            waveNumber++;
            SetWaveOnScreen(waveNumber);
            StartCoroutine(Wave(waveNumber));
            allDead = false;
        }
    // Si le joueur appuis sur Echap alors la valeur de isPaused devient le contraire.
		if(Input.GetKeyDown(KeyCode.Escape))
			isPaused = !isPaused;


		if(isPaused)
			Time.timeScale = 0f; // Le temps s'arrete
		
		else
			Time.timeScale = 1.0f; // Le temps reprend


	}

	void OnGUI ()
	{
		if(isPaused)
		{
		
			if(GUI.Button(new Rect(Screen.width / 2 - 40, Screen.height / 2 - 20, 80, 40), "Reprendre"))
			{
				isPaused = false;
			}

			if(GUI.Button(new Rect(Screen.width / 2 - 40, Screen.height / 2 + 40, 80, 40), "Abandonner"))
			{
                Loose();
                SceneManager.LoadScene(0);
			}

		}
	}

    public void NotifyEnnemyDeath()
    {
        nbDeath++;
    }

    public int fibo(int n) { return fiboValues[n]; }

    
    private IEnumerator Wave(int wave_number)
    {
        nbDeath = 0;
        allDead = false;
        yield return new WaitForSeconds(3f); //wait un peu avant de lancer la vague
        int n = fibo(wave_number);
        for(int i = 0; i < n; i++)
        {
            SpawnRandomEnnemi(); //Le fait en fonction d'un list de spawns
                                //type d'ennemi aléatoire
            yield return new WaitForSeconds(1f);
        }
        
        while(nbDeath < n)
        {
            yield return new WaitForEndOfFrame(); //Wait here til all dead
        }
        allDead = true;
    }


    public void SpawnRandomEnnemi()
    {
        var array = EnnemiType.GetValues(typeof(EnnemiType));
        EnnemiType type = (EnnemiType)array.GetValue(Random.Range(0,array.Length));
        GameObject ennemi = enemyFactory.Get(type);
        
        int index = Random.Range(0,spawnsList.Count);
        Vector3 spawnPosition = (Vector3)spawnsList[index];
        ennemi.gameObject.transform.position = spawnPosition;
        ennemi.gameObject.SetActive(true);
    }

    //UI PArt
    public void UpdateCoolDownOnScreen(float cooldownFirst,float cooldownSecond, float maxCooldown1, float maxCooldown2)
    {
        coolDown1.fillAmount = cooldownFirst/maxCooldown1;
        coolDown2.fillAmount = cooldownSecond/maxCooldown2;
    }

    public void SetWaveOnScreen(int waveNb)
    {
        waveText.text = "Vague N°" + waveNb.ToString();
    }

    public void SetLifeSliderMaxValue(int maxHealth)
    {
        lifeSlider.maxValue = maxHealth;
        lifeSlider.value = maxHealth;
    }

    public void UpdateLifeOnScreen(int health)
    {
        /*
        fillRect = lifeSlider.fillRect;
        curValue = lifeSlider.value;
        targetValue = health;
        curValue = Mathf.MoveTowards(curValue, targetValue, Time.deltaTime);
        Vector2 fillAnchor = fillRect.anchorMax;
        fillAnchor.x = Mathf.Clamp01(curValue/lifeSlider.maxValue);
        fillRect.anchorMax = fillAnchor;
        lifeSlider.GetComponent<Image>().color = Color.Lerp(Color.green,Color.red, lifeSlider.value / lifeSlider.maxValue);
        */
        lifeSlider.value = health;
    }

    public void AddScore(int nbEnnemies)
    {
        score += nbEnnemies;
        scoreText.text = "Score : " + score.ToString();
    }

    public void StartCounting()
    {
        startCounting = true;
    }

    public void Win()
    {
        if(best_score<= score)
        {
            PlayerPrefs.SetInt("BestScore"+playerSelected.ToString(),score);
            PlayerPrefs.SetInt("BestWave"+playerSelected.ToString(),waveNumber);
        }
        PlayerPrefs.SetInt("NbPlayed"+playerSelected.ToString(),PlayerPrefs.GetInt("NbPlayed"+playerSelected.ToString(),0)+1);
        popup.SetActive(true);
        popupText.text = "Gagné !";
    }

    public void Loose()
    {
        if(best_score<= score)
        {
            PlayerPrefs.SetInt("BestScore"+playerSelected.ToString(),score);
            PlayerPrefs.SetInt("BestWave"+playerSelected.ToString(),waveNumber);
        }
        PlayerPrefs.SetInt("NbPlayed"+playerSelected.ToString(),PlayerPrefs.GetInt("NbPlayed"+playerSelected.ToString(),0)+1);
        popup.SetActive(true);
        popupText.text = "Perdu !";
    }
}
