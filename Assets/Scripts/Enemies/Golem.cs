﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Golem : EnnemiCharacter
{ 
    private Vector3 target;

    [SerializeField]
    private ParticleSystem ps;
    private bool isStomping = false;
    private float stompForce = 0.3f; //high since it is multiplied with the damages
    public override void Start()
    {
        base.Start();
        speed = (float)EnnemiSpeed.SLOW;
        action_cooldown = 5f;
        health = 50;

        target = new Vector3(Random.Range(0, ArenaMaker.xSize * 2), transform.position.y, Random.Range(0,ArenaMaker.ySize));
    }
    public override void Update()
    {
        base.Update();
        if(action_timer >= action_cooldown)
        {
            action_timer = 0f;
            isStomping = true;
            Action();
        }
    }

    public override void FixedUpdate()
    {

        if(!isStomping)
        {
            Vector3 feet_pos = new Vector3(transform.position.x,0f,transform.position.z); //fixes so minor bugs
            float angle = Vector3.SignedAngle(Vector3.forward, target - feet_pos, Vector3.up);
            transform.rotation = Quaternion.Euler(new Vector3(0f, angle, 0f));

            if(Vector3.Distance(target, transform.position) > 1f)
            {
                if(!isStun)
                {
                    _rigidbody.velocity = transform.forward * speed + Vector3.up * _rigidbody.velocity.y;
                }
                animator.SetFloat("vertical", Vector3.Dot(_rigidbody.velocity, transform.forward));
                animator.SetFloat("horizontal", Vector3.Dot(_rigidbody.velocity, transform.right));
            }
            else
            {
                _rigidbody.velocity *= 0.9f;
                animator.SetFloat("vertical", 0f);
                animator.SetFloat("horizontal", 0f);
            }
        }
    }

    public override void Action()
    {
        StartCoroutine(Stomp());
        target = new Vector3(Random.Range(0, ArenaMaker.xSize * 2), transform.position.y, Random.Range(0,ArenaMaker.ySize));
    }

    IEnumerator Stomp()
    {
        animator.SetTrigger("roar");
        yield return new WaitForSeconds(2f);
        //Stomp starts here
        yield return new WaitForSeconds(0.6f);

        Collider[] cols = Physics.OverlapSphere(transform.position, 5);
        foreach(Collider col in cols)
        {
            if(col.CompareTag("Player") && col.isTrigger)
            {
                PlayableCharacter script = col.gameObject.GetComponent<PlayableCharacter>();
                int damage = (int)(100 * ((15f - Vector3.Distance(col.transform.position, transform.position)) / 15f));
                script.Hurt(damage);

                Vector3 force = col.transform.position - transform.position;
                force = force.normalized;
                script.AddForce(force * (damage * stompForce));
            }
        }
        ps.Play();

        yield return new WaitForSeconds(0.4f);
        action_timer = 0f;
        isStomping = false;
    }
}
