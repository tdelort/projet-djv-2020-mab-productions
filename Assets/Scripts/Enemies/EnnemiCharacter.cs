﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum EnnemiSpeed
{
    SLOW = 3,
    MEDIUM = 5,
    FAST = 8,
}

public abstract class EnnemiCharacter : MonoBehaviour
{
    protected int health;
    protected float speed;
    protected float action_cooldown;
    protected float action_timer = 0f;

    protected GameObject player;

    protected Rigidbody _rigidbody;

    protected Animator animator;
    
    protected bool isStun = false;
    protected float stunTime = 0.7f;
    public abstract void Action();

    protected bool isBlocked = false;
    protected Vector3 blockedPosition;
    protected GameBehaviour gb;

    public virtual void Awake()
    {
        gb = GameObject.Find("GameBehavior").GetComponent<GameBehaviour>();
    }

    public virtual void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        _rigidbody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    public virtual void Update()
    {
        if (health <= 0){
            Die();
        }
        action_timer += Time.deltaTime;
    }

    public virtual void FixedUpdate()
    {
        if(isBlocked)
        {
            _rigidbody.constraints = RigidbodyConstraints.FreezePosition;
        }
    }

    protected void DoRotation()
    {
        Vector3 target = player.transform.position;
        Vector3 feet_pos = new Vector3(transform.position.x,0f,transform.position.z); //fixes so minor bugs
        float angle = Vector3.SignedAngle(Vector3.forward, target - feet_pos, Vector3.up);
        transform.rotation = Quaternion.Euler(new Vector3(0f, angle, 0f));
    }
    
    public void Die()
    {
        gb.NotifyEnnemyDeath();
        gb.AddScore(1);
        gameObject.SetActive(false);
        enabled = false; //Stops the script
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("hit");
        switch (other.tag)
        {
            case "Arrow" :
                health -= 10;
                break;
            case "Trap":
                _rigidbody.constraints = RigidbodyConstraints.FreezePosition;
                break;
            default:
                break;
        }
    }
    public void Hurt(int damage)
    {
        Debug.Log("ennemi took : " + damage);
        health -= damage;
    }
    public void AddForce(Vector3 force)
    {
        isStun = true;
        _rigidbody.velocity = Vector3.zero;
        _rigidbody.AddForce(force,ForceMode.VelocityChange);
        StartCoroutine(Stun());
    }

    IEnumerator Stun()
    {
        yield return new WaitForSeconds(stunTime);
        isStun = false;
    }

    public void Block()
    {
        isBlocked = true;
    }

}
