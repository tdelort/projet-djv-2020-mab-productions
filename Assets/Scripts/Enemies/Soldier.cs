﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soldier : EnnemiCharacter
{
    private float hit_distance = 2f;

    [SerializeField]
    private Collider slashHitBox;

    public override void Start()
    {
        base.Start();
        speed = (float)EnnemiSpeed.MEDIUM;
        action_cooldown = 1f;
        health = 10;
    }
    public override void Update()
    {
        base.Update();
        DoRotation();
    }

    public override void FixedUpdate()
    {
        _rigidbody.rotation = transform.rotation;

        if (Vector3.Distance(player.transform.position,transform.position) > hit_distance)
        {
            if(!isStun)
            {
                _rigidbody.velocity = transform.forward * speed + Vector3.up * _rigidbody.velocity.y;
            }
            animator.SetFloat("vertical", Vector3.Dot(_rigidbody.velocity, transform.forward));
            animator.SetFloat("horizontal", Vector3.Dot(_rigidbody.velocity, transform.right));

        }
        else
        {
            _rigidbody.velocity *= 0.9f;
            animator.SetFloat("vertical", 0f);
            animator.SetFloat("horizontal", 0f);

            //He attacc
            Action();
        }

    }

    public override void Action()
    {
        if(action_timer >= action_cooldown)
        {
            action_timer = 0f;
            StartCoroutine(Slash());
        }
    }

    IEnumerator Slash()
    {
        if(Random.Range(0f,1f) > 0.5f)
            animator.SetTrigger("slash");
        else
            animator.SetTrigger("slash2");

        yield return new WaitForSeconds(0.5f); //waits for the animation start a bit

        Collider[] cols = Physics.OverlapSphere(transform.position + transform.forward * 2 + transform.up * 2, 2);
        foreach(Collider col in cols)
        {
            if(col.CompareTag("Player") && col.isTrigger)
            {
                PlayableCharacter script = col.gameObject.GetComponent<PlayableCharacter>();
                int damage = 5;
                script.Hurt(damage);
            }
        }
    }

}
