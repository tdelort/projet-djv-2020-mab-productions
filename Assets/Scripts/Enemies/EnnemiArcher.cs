﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemiArcher : EnnemiCharacter
{
    private ArcherObjectsFactory factory;
    private Vector3 target;
    private bool isShooting = false;
    
    public override void Start()
    {
        base.Start();
        speed = (float)EnnemiSpeed.FAST;
        action_cooldown = 5f;
        health = 5;
        factory = GameObject.Find("ArcherFactory").GetComponent<ArcherObjectsFactory>(); //no need to link it

        target = new Vector3(Random.Range(0, ArenaMaker.xSize * 2), transform.position.y, Random.Range(0,ArenaMaker.ySize));
    }

    public override void Action()
    {
        if(action_timer >= action_cooldown)
        {
            isShooting = true;
            target = new Vector3(Random.Range(0, ArenaMaker.xSize * 2), transform.position.y, Random.Range(0,ArenaMaker.ySize));
            action_timer = 0f;
            StartCoroutine(Shoot());
        }
    }

    public override void Update()
    {
        base.Update();
        Action(); // will set isShooting to true if cooldown finished

    }

    public override void FixedUpdate()
    {
        if (!isShooting)
        {
            Vector3 feet_pos = new Vector3(transform.position.x,0f,transform.position.z); //fixes so minor bugs
            float angle = Vector3.SignedAngle(Vector3.forward, target - feet_pos, Vector3.up);
            transform.rotation = Quaternion.Euler(new Vector3(0f, angle, 0f));

            if (Vector3.Distance(target, transform.position) > 1f)
            {
                if(!isStun)
                {
                    _rigidbody.velocity = transform.forward * speed + Vector3.up * _rigidbody.velocity.y;
                }
                animator.SetFloat("vertical", Vector3.Dot(_rigidbody.velocity, transform.forward));
                animator.SetFloat("horizontal", Vector3.Dot(_rigidbody.velocity, transform.right));
            }
            else
            {
                _rigidbody.velocity *= 0.9f;
                animator.SetFloat("vertical", 0f);
                animator.SetFloat("horizontal", 0f);
            }
        }
    }

    IEnumerator Shoot()
    {
        int arrows = Random.Range(1, 6); //1 to 5
        for(int i = 0; i < arrows; i++)
        {
            Vector3 target = player.transform.position;

            Vector3 feet_pos = new Vector3(transform.position.x,0f,transform.position.z); //fixes so minor bugs
            float angle = Vector3.SignedAngle(Vector3.forward, target - feet_pos, Vector3.up);
            transform.rotation = Quaternion.Euler(new Vector3(0f, angle, 0f));

            Debug.Log("piou");
            ShootOne();
            yield return new WaitForSeconds(0.5f);
        }
        isShooting = false;
    }

    public void ShootOne()
    {
        animator.SetTrigger("fire");
        GameObject arrow = factory.GetArrow(true);
        arrow.transform.rotation = transform.rotation;
        arrow.transform.position = transform.position;
        arrow.transform.position += new Vector3(0, 1, 0);

        //important, sets the factory which can pool the arrow when ded)
        Arrow arrow_script = arrow.GetComponent<Arrow>();
        arrow_script.SetFactory(factory);
        //the arrow will then fly to its destiny
    }
}
