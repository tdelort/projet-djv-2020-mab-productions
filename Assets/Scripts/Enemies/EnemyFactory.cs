﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnnemiType
{
  ARCHER,
  SOLDIER,
  GOLEM
}

public class EnemyFactory : MonoBehaviour
{

    [SerializeField] private GameObject archer_prefab;
    [SerializeField] private GameObject soldier_prefab;
    [SerializeField] private GameObject golem_prefab;

    public GameObject Get(EnnemiType type)
    {
        GameObject obj;
        switch(type)
        {
        case EnnemiType.SOLDIER : 
            obj = Instantiate(soldier_prefab);
            break;
        case EnnemiType.ARCHER : 
            obj = Instantiate(archer_prefab);
            break;
        case EnnemiType.GOLEM : 
            obj = Instantiate(golem_prefab);
            break;
        default :
            obj = Instantiate(soldier_prefab);
            break;
        }
        return obj;
    }

}
