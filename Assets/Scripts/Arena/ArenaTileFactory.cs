﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public enum TileType
{
    SIMPLE,
    WALL,
    HOLE,
    EXTWALL
}

public class ArenaTileFactory : MonoBehaviour
{
    [SerializeField]
    private GameObject wallTile;

    [SerializeField]
    private GameObject simpleTile;

    [SerializeField]
    private GameObject holeTile;

    [SerializeField]
    private GameObject extWallTile;

    [SerializeField]
    private GameObject arena;

    public GameObject GetTile(TileType type, int x = 0, int z = 0)
    {
        GameObject tile;
        switch (type)
        {
            case TileType.SIMPLE:
                tile = Instantiate(simpleTile);
                break;
            case TileType.WALL:
                tile = Instantiate(wallTile);
                break;
            case TileType.HOLE:
                tile = Instantiate(holeTile);
                break;
            case TileType.EXTWALL:
                tile = Instantiate(extWallTile);
                break;
            default:
                tile = Instantiate(simpleTile);
                break;
        }
        tile.transform.parent = arena.transform;
        tile.transform.position = new Vector3(2 * x, tile.transform.position.y, 2 * z);
        return tile;
    }
}
