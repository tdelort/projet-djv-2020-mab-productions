﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArenaMaker : MonoBehaviour
{
    [SerializeField]
    ArenaTileFactory factory;

    private TileType[,] arenaTypes;
    private GameObject[,] arenaTiles;

    // accessible from anywhere
    // PLEASE let them even (odd will make the central spawn point weird)
    public static int xSize = 24;
    public static int ySize = 24; 

    private void Awake()
    {
        arenaTypes = new TileType[xSize, ySize];
        arenaTiles = new GameObject[xSize, ySize];

        // Algo to choose tile type goes here
        for (int x = 0; x < xSize; x++)
        {
            for (int y = 0; y < ySize; y++)
            {
                // Make outside walls
                if(x == 0 || x == xSize - 1 || y == 0 || y == ySize - 1)
                {
                    arenaTypes[x, y] = TileType.EXTWALL;
                }
                else
                {
                    // inner cells
                    if (Random.Range(0f, 1f) > 0.3)
                        arenaTypes[x, y] = TileType.SIMPLE;
                    else
                    {
                        if (Random.Range(0f,1f) < 0.5)
                            arenaTypes[x, y] = TileType.HOLE;
                        else
                            arenaTypes[x, y] = TileType.WALL;
                    }   
                }
            }
        }
        //Player Spawn point
        arenaTypes[xSize / 2, ySize / 2] = TileType.SIMPLE;

        //Condition verification
        for (int x = 1; x < xSize - 1; x++)
        {
            for (int y = 1; y < ySize - 1; y++)
            {
                int specialAdjTiles = 0;
                if (arenaTypes[x - 1, y - 1] == TileType.HOLE || arenaTypes[x - 1, y - 1] == TileType.WALL) specialAdjTiles += 1;
                if (arenaTypes[x - 1, y] == TileType.HOLE || arenaTypes[x - 1, y] == TileType.WALL) specialAdjTiles += 1;
                if (arenaTypes[x - 1, y + 1] == TileType.HOLE || arenaTypes[x - 1, y + 1] == TileType.WALL) specialAdjTiles += 1;
                if (arenaTypes[x, y - 1] == TileType.HOLE || arenaTypes[x, y - 1] == TileType.WALL) specialAdjTiles += 1;
                if (arenaTypes[x, y + 1] == TileType.HOLE || arenaTypes[x, y + 1] == TileType.WALL) specialAdjTiles += 1;
                if (arenaTypes[x + 1, y - 1] == TileType.HOLE || arenaTypes[x + 1, y - 1] == TileType.WALL) specialAdjTiles += 1;
                if (arenaTypes[x + 1, y] == TileType.HOLE || arenaTypes[x + 1, y] == TileType.WALL) specialAdjTiles += 1;
                if (arenaTypes[x + 1, y + 1] == TileType.HOLE || arenaTypes[x + 1, y + 1] == TileType.WALL) specialAdjTiles += 1;
                if (specialAdjTiles > 2)
                {
                    arenaTypes[x, y] = TileType.SIMPLE;
                }
            }
        }

        // Instantiating tiles
        for (int x = 0; x < xSize; x++)
        {
            for (int y = 0; y < ySize; y++)
            {
                arenaTiles[x, y] = factory.GetTile(arenaTypes[x, y],x,y);
            }
        }

    }

    public List<Vector3> GetSpawns()
    {
        List<Vector3> spawnsList =  new List<Vector3>();

        int nbCollums = arenaTypes.GetLength(0);
        int nbLines = arenaTypes.GetLength(1);

        int nbSpawns = Random.Range(4,8);

        int index_x = 0 ;
        int index_y = 0;

        for(int i =0;i<nbSpawns;i++)
        {
            TileType tmp = TileType.HOLE;
            while (tmp != TileType.SIMPLE){
                index_x = Random.Range(0,nbCollums);
                index_y = Random.Range(0,nbLines);
                tmp =arenaTypes[index_x, index_y];
            }
            int x = 2*index_x;
            int z = 2*index_y;
            spawnsList.Add(new Vector3(x,0,z));

        }
        
        return spawnsList;
    }


}
