﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Archer : PlayableCharacter
{
    private ArcherObjectsFactory factory;
    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        speed = 5f;
        health = 30;
        playCharSoundTimer = 0.4f;
        animator = GetComponent<Animator>();
        _rigidbody = GetComponent<Rigidbody>();
        factory = GameObject.Find("ArcherFactory").GetComponent<ArcherObjectsFactory>(); //no need to link it
        Debug.Log(factory);

        action_cooldown = new float[] { 1f, 3f, 10f };
        for (int i = 0; i < nb_actions; i++)
        {
            action_timer[i] = action_cooldown[i]; //all actions doable on start
        }
    }

    public override void Action(int action_number)
    {
        //Check if cooldown passed
        if (action_timer[action_number - 1] < action_cooldown[action_number - 1])
            return;

        action_timer[action_number - 1] = 0f;

        // routes to appropriate action
        switch (action_number)
        {
            case 1:
                Shoot();
                break;
            case 2:
                Trap();
                break;
            case 3:
                StartCoroutine(Hate());
                break;
            default:
                Debug.Log(action_number);
                break;
        }
    }

    public void Shoot()
    {
        animator.SetTrigger("fire");
        GameObject arrow = factory.GetArrow(false);
        arrow.transform.rotation = transform.rotation;
        arrow.transform.position = transform.position;
        arrow.transform.position += new Vector3(0, 1, 0);

        //important, sets the factory which can pool the arrow when ded)
        Arrow arrow_script = arrow.GetComponent<Arrow>();
        arrow_script.SetFactory(factory);
        //the arrow will then fly to its destiny
    }

    public void Trap()
    {
        animator.SetTrigger("drop");
        GameObject trap = factory.GetTrap();
        trap.transform.position = transform.position;
    }

    IEnumerator Hate()
    {
        float old_cooldown = action_cooldown[0];
        action_cooldown[0] = 0.5f;
        speed *= 2;

        yield return new WaitForSeconds(3f);

        speed /= 2;
        action_cooldown[0] = old_cooldown;
    }
}
