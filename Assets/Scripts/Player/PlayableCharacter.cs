﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerType 
{
    KNIGHT,
    BOWMAN,
    MAGE
}

public abstract class PlayableCharacter : MonoBehaviour
{
    public int health;
    protected float speed;
    protected float acc = 3f;

    protected Animator animator;

    protected float h_axis;
    protected float v_axis;
    protected bool isWalking = false;
    protected float sound_timer = 0f;
    protected float playCharSoundTimer = 0.6f;

    protected Rigidbody _rigidbody; 
    protected Vector3 mouse_pos_in_world;
    protected Ray ray_mouse;
    protected AudioSource audioData;

    [SerializeField]
    protected GameObject cursor;

    protected GameBehaviour gb;

    //An array since we migh want to add more than 3 actions in the future.
    public static int nb_actions = 3;
    protected float[] action_cooldown = new float[nb_actions];
    protected float[] action_timer = new float[] { 0f, 0f, 0f };

    protected bool isStun = false;
    protected float stunTime = 0.7f;

    // action_number parameter is here to choose the action we want to execute
    public abstract void Action(int action_number);

    // Start and Update set as Virtual :
    // If deemed not good enough in Children, they will be Overrideable
    // It like an optionnally abstract method. It has a implementation but you may need to override it
    public virtual void Start()
    {
        transform.position = new Vector3(ArenaMaker.xSize,0.5f, ArenaMaker.ySize);
        gb = GameObject.Find("GameBehavior").GetComponent<GameBehaviour>();
        audioData = GetComponent<AudioSource>();
        Physics.IgnoreLayerCollision(0, 8, false); //resets the collision with holes
        Physics.IgnoreLayerCollision(0, 10, false); //resets the collision with holes
    }
    
    public void ResetPos()
    {
        transform.position = new Vector3(ArenaMaker.xSize,0.5f, ArenaMaker.ySize);
    }

    public virtual void FixedUpdate()
    {

        if (Time.timeScale == 0)
            return;

        // physics related movements
        if (h_axis != 0f || v_axis != 0f)
        {
            isWalking = true;
            if(!isStun)
            {
                _rigidbody.velocity = new Vector3(
                    Mathf.Clamp(h_axis * speed, -speed, speed),
                    _rigidbody.velocity.y,
                    Mathf.Clamp(v_axis * speed, -speed, speed)
                );
            }
            animator.SetFloat("vertical", Vector3.Dot(_rigidbody.velocity, transform.forward));
            animator.SetFloat("horizontal", Vector3.Dot(_rigidbody.velocity, transform.right));
        }
        else
        {
            isWalking = false;
            _rigidbody.velocity *= 0.9f;
            animator.SetFloat("vertical", 0f);
            animator.SetFloat("horizontal", 0f);
        }

    }

    public virtual void Update()
    {
        if (Time.timeScale == 0)
            return;
        sound_timer+=Time.deltaTime;
        if(isWalking && !isStun && sound_timer>=playCharSoundTimer)
        {
            sound_timer=0f;
            StartCoroutine(PlayWalkingSound());
        }
        else if(!isWalking)
        {
            StopCoroutine(PlayWalkingSound());
        }
        if(health <= 0)
            Death();
        //Inputs and movements not physics related
        h_axis = Input.GetAxis("Horizontal");
        v_axis = Input.GetAxis("Vertical");

        DoRotation();

        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.Space))
        {
            Action(3);
        }
        else if (Input.GetMouseButtonDown(0))
        {
            Action(1);
        }
        else if (Input.GetMouseButtonDown(1))
        {
            Action(2);
        }
        else if (Input.GetKeyDown(KeyCode.M))
        {
            gameObject.GetComponent<CapsuleCollider>().enabled = !gameObject.GetComponent<CapsuleCollider>().enabled;
        }

        for(int i = 0; i < nb_actions; i++)
        {
            action_timer[i] += Time.deltaTime;
        }

        gb.UpdateLifeOnScreen(health);
        gb.UpdateCoolDownOnScreen(action_timer[1],action_timer[2],action_cooldown[1],action_cooldown[2]);
    }

    private void DoRotation()
    {
        Plane plane = new Plane(Vector3.up, 0);

        float distance_mouse;
        ray_mouse = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (plane.Raycast(ray_mouse, out distance_mouse))
        {
            mouse_pos_in_world = ray_mouse.GetPoint(distance_mouse);
            cursor.transform.position = mouse_pos_in_world;
        }
        Vector3 feet_pos = new Vector3(transform.position.x,0f,transform.position.z); //fixes so minor bugs
        float angle = Vector3.SignedAngle(Vector3.forward, mouse_pos_in_world - feet_pos, Vector3.up);
        transform.rotation = Quaternion.Euler(new Vector3(0f, angle, 0f));
    }

    // setters and getters
    public int getHealth()
    {
        return this.health;
    }

    public float getSpeed()
    {
        return this.speed;
    }

    public void setHealth(int HP)
    {
        this.health = HP;
    }
    public void setSpeed(float newSpeed)
    {
        this.speed = newSpeed;
    }

    public Animator getAnimator()
    {
        return this.animator;
    }
    public void setAnimator(Animator newAnimator)
    {
        this.animator = newAnimator;
    }

    private void OnTriggerEnter(Collider other)
    {
        switch (other.tag)
        {
            case "EnnemiArrow" :
                health -= 5;
                break;
            default:
                break;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.layer == 10)
        {
            Death();
        }
    }

    public void Hurt(int damage)
    {
        Debug.Log("took : " + damage);
        health -= damage;
    }

    public void AddForce(Vector3 force)
    {
        isStun = true;
        _rigidbody.velocity = Vector3.zero;
        _rigidbody.AddForce(force, ForceMode.VelocityChange);
        StartCoroutine(Stun());
    }

    IEnumerator Stun()
    {
        yield return new WaitForSeconds(stunTime);
        isStun = false;
    }

    private IEnumerator PlayWalkingSound()
    {
        audioData.Play(0);
        yield return new WaitForSecondsRealtime(0.5f);
    }

    public void Death()
    {
        Debug.Log("ded");
        gb.Loose();
        gameObject.SetActive(false);
        enabled = false;
    }
}
