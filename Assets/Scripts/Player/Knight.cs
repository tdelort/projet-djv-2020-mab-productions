﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knight : PlayableCharacter
{
    private Animator knightAnimator;
    private float jumpForce = 20f;

    private float leapForce = 6f;
    private float slashForce = 3f;
    [SerializeField]
    private ParticleSystem ps;

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        speed = 5f;
        health = 50;
        playCharSoundTimer = 0.38f;
        knightAnimator = GetComponent<Animator>();
        _rigidbody = GetComponent<Rigidbody>();
        
        setAnimator(knightAnimator);

        action_cooldown = new float[] { 1f, 5f, 5f };
        for (int i = 0; i < nb_actions; i++)
        {
            action_timer[i] = action_cooldown[i]; //all actions doable on start
        }
    }

    public override void Action(int action_number)
    {
        //Check if cooldown passed
        if (action_timer[action_number - 1] < action_cooldown[action_number - 1])
            return;

        action_timer[action_number - 1] = 0f;
        
        // routes to appropriate action
        switch(action_number)
        {
            case 1:
                StartCoroutine(Slash());
                break;
            case 2:
                StartCoroutine(Leap());
                break;
            case 3:
                StartCoroutine(Spin());
                break;
            default:
                Debug.Log(action_number);
                break;
        }
    }
    
    //Action 1
    IEnumerator Slash()
    {
        if(Random.Range(0f,1f) > 0.5f)
            knightAnimator.SetTrigger("slash");
        else
            knightAnimator.SetTrigger("slash2");

        yield return new WaitForSeconds(0.5f); //waits for the animation start a bit

        Collider[] cols = Physics.OverlapSphere(transform.position + transform.forward * 2 + transform.up * 2, 2);
        foreach(Collider col in cols)
        {
            if(col.CompareTag("Ennemi") && col.isTrigger)
            {
                EnnemiCharacter script = col.gameObject.GetComponent<EnnemiCharacter>();
                int damage = 5;
                script.Hurt(damage);
                Vector3 force = col.transform.position - transform.position;
                force = force.normalized;
                script.AddForce(force * (damage * slashForce));
            }
        }
    }

    //Action 2
    IEnumerator Leap()
    {
        knightAnimator.SetTrigger("jump");
        yield return new WaitForSeconds(0.1f);
        Physics.IgnoreLayerCollision(0, 8, true); //ignore collision with Hole layer (cross holes)
        Physics.IgnoreLayerCollision(0, 10, true); //ignore collision with Hole layer (cross holes)

        Vector3 target = cursor.transform.position;
        target.y = transform.position.y;
        while(transform.position != target)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, jumpForce * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
        
        Physics.IgnoreLayerCollision(0, 10, false); //resets the collision with holes
        Physics.IgnoreLayerCollision(0, 8, false); //resets the collision with holes
        ps.Play();
        Collider[] cols = Physics.OverlapSphere(transform.position, 15);
        foreach(Collider col in cols)
        {
            if(col.CompareTag("Ennemi") && col.isTrigger)
            {
                EnnemiCharacter script = col.gameObject.GetComponent<EnnemiCharacter>();
                int damage = 2;
                script.Hurt(damage);

                Vector3 force = col.transform.position - transform.position;
                force = force.normalized;
                script.AddForce(force * (damage * leapForce));
            }
        }

    }

    //Action 3
    IEnumerator Spin()
    {
        knightAnimator.SetTrigger("spin");
        speed *= 1.5f;

        // to catch these collision, use OnTriggerEnter
        for (int i = 0; i < 6; i++)
        {
            Collider[] cols = Physics.OverlapSphere(transform.position, 5);
            foreach(Collider col in cols)
            {
                if(col.CompareTag("Ennemi"))
                {
                    EnnemiCharacter script = col.gameObject.GetComponent<EnnemiCharacter>();
                    int damage = 1;
                    script.Hurt(damage);
                }
            }
            yield return new WaitForSeconds(0.4f);
        }

        speed /= 1.5f;
        knightAnimator.SetTrigger("unSpin");
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        //Use the same vars you use to draw your Overlap SPhere to draw your Wire Sphere.
        Gizmos.DrawWireSphere(transform.position + transform.forward * 2 + transform.up * 2, 2);
        Gizmos.DrawWireSphere(transform.position, 5);
    }
}
