﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player1 : PlayableCharacter
{
    // Start is called before the first frame update
    public override void Start()
    {
        if(GetComponent<Rigidbody> () == null) //a voir si on en a besoin
        {
            _rigidbody = gameObject.AddComponent<Rigidbody>();
        }
        else
        {
            _rigidbody = GetComponent<Rigidbody>();
        }
        setSpeed(5f);
    }
    

    // Update is called once per frame
    
    /*public void Update()
    {
        
    }*/

    public override void Action(int action_number)
    {
        Debug.Log(action_number);
    }
}
