﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mage : PlayableCharacter
{
    private Animator mageAnimator;
    private AudioSource audioDataWall;

    private MageFactory factory;
    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        speed = 5f;
        health = 40;
        playCharSoundTimer = 0.6f;
        mageAnimator = GetComponent<Animator>();
        _rigidbody = GetComponent<Rigidbody>();
        factory = GetComponentInChildren<MageFactory>();

        setAnimator(mageAnimator);
        action_cooldown = new float[] { 2f, 10f, 5f };
        for (int i = 0; i < nb_actions; i++)
        {
            action_timer[i] = action_cooldown[i]; //all actions doable on start
        }
    }

    public override void Action(int action_number)
    {
        if (action_timer[action_number - 1] < action_cooldown[action_number - 1])
            return;

        action_timer[action_number - 1] = 0f;

        // routes to appropriate action
        switch (action_number)
        {
            case 1:
                Fireball();
                break;
            case 2:
                Wall();
                break;
            case 3:
                Switch();
                break;
            default:
                Debug.Log(action_number);
                break;
        }
    }

    public void Fireball()
    {
        GameObject ball = factory.Get(MageObject.FIREBALL);
        ball.transform.rotation = transform.rotation;
        ball.transform.position = transform.position + transform.up + transform.forward;
        FireBall script = ball.GetComponent<FireBall>();
        script.Setfactory(factory);
        script.Launch();
    }
    public void Wall()
    {
        //need to check if on hole here
        GameObject wall = factory.Get(MageObject.WALL);
        wall.transform.rotation = transform.rotation;
        wall.transform.position = mouse_pos_in_world + Vector3.up;
        audioDataWall = wall.GetComponent<AudioSource>();
        audioDataWall.Play(0);
        StartCoroutine(DestroyWall(wall));
    }

    IEnumerator DestroyWall(GameObject wall)
    {
        yield return new WaitForSeconds(5f);
        factory.Delete(wall, MageObject.WALL);
    } 

    public void Switch()
    {
        Collider[] cols = Physics.OverlapSphere(mouse_pos_in_world, 2);
        foreach(Collider col in cols)
        {
            if(col.CompareTag("Ennemi") && col.isTrigger)
            {
                //swapping positions
                (transform.position, col.gameObject.transform.position) = (col.gameObject.transform.position, transform.position);
                return;
            }
        }
    }
}
