﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    private PlayerType playerType;

    public static GameManager instance;


    public void setPlayerType(PlayerType type)
    {
        playerType = type;
    }

    public PlayerType getPlayerType()
    {
        return playerType;
    }

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this);
        }
    }
}
