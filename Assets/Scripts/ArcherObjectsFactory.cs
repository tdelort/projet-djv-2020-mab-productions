﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ArcherObject
{
    ARROW,
    TRAP
}

public class ArcherObjectsFactory : MonoBehaviour
{
    [SerializeField]
    private GameObject arrow_prefab;
    [SerializeField]
    private GameObject trap_prefab;

    private Queue<GameObject> arrow_pool = new Queue<GameObject>();
    private Queue<GameObject> trap_pool = new Queue<GameObject>();

    public GameObject GetArrow(bool isEnnemi)
    {
        GameObject obj;
        if(arrow_pool.Count != 0)
        {
            obj = arrow_pool.Dequeue();
            obj.SetActive(true);
        }
        else
        {
            obj = Instantiate(arrow_prefab);
        }

        if(isEnnemi)
            obj.tag = "EnnemiArrow";
        else
            obj.tag = "Arrow";

        return obj;
    }

    public void DeleteArrow(GameObject arrow)
    {
        arrow.SetActive(false);
        arrow_pool.Enqueue(arrow);
    }

    public GameObject GetTrap()
    {
        GameObject obj;
        if(trap_pool.Count != 0)
        {
            obj = trap_pool.Dequeue();
        }
        else
        {
            obj = Instantiate(trap_prefab);
        }
        return obj;
    }
    public void DeleteTrap(GameObject trap)
    {
        trap.SetActive(false);
        trap_pool.Enqueue(trap);
    }
}
