﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    private Transform target;
    bool targetSet = false;

    [SerializeField]
    private Vector3 distance = new Vector3(0,5,5);
    
    public void SetTarget(Transform t)
    {
        target = t;
        targetSet = true;
    }

    void Update()
    {
        if(targetSet)
            transform.position = target.position - distance;
    }
}
