﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public Button startButton;
    public Button paramButton;
	public Button quitButton;
    
	void Start () {
		quitButton.onClick.AddListener(()=>Quit());
		startButton.onClick.AddListener(StartGame);
        paramButton.onClick.AddListener(GoToSettings);
	}

	void StartGame(){
		Debug.Log ("Lancement de la sélection du personnage");
        SceneManager.LoadScene(1);
	}

    void GoToSettings()
    {
        Debug.Log("Going to settings");
        SceneManager.LoadScene(2);
    }

	private void Quit()
    {
        Debug.Log("Bye Bye!");
        Application.Quit();
    }

}
