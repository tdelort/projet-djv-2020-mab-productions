﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class SelectPlayerManager : MonoBehaviour
{
    [SerializeField]
    public Button mageButton;
    [SerializeField]
    public Button knightButton;
    [SerializeField]
    public Button bowmanButton;
    [SerializeField]
	public Button backButton;

    
    public Text bowText;
    public Text mageText;
    public Text knightText;

    private Dictionary<int,int> best_scores =new Dictionary<int,int>();
    private Dictionary<int,int> best_waves =new Dictionary<int,int>();
    private Dictionary<int,int> nb_played =new Dictionary<int,int>();
    
    
	void Start () {
		backButton.onClick.AddListener(()=>Back());
		mageButton.onClick.AddListener(()=>SetMage());
        bowmanButton.onClick.AddListener(()=>SetBowman());
        knightButton.onClick.AddListener(()=>SetKnight());
	}

    private void Update(){
        
        foreach (PlayerType player in (PlayerType[]) PlayerType.GetValues(typeof(PlayerType)))
        {
            nb_played[(int) player] = PlayerPrefs.GetInt("NbPlayed"+player.ToString(),0);
            best_waves[(int) player] = PlayerPrefs.GetInt("BestWave"+player.ToString(),0);
            best_scores[(int) player] = PlayerPrefs.GetInt("BestScore"+player.ToString(),0);
        }

        bowText.text = "Vague Record : "+best_waves[(int) PlayerType.BOWMAN]+"\nScore : "+best_scores[(int) PlayerType.BOWMAN]+"\nNombre de parties : "+nb_played[(int) PlayerType.BOWMAN];
        mageText.text = "Vague Record : "+best_waves[(int) PlayerType.MAGE]+"\nScore : "+best_scores[(int) PlayerType.MAGE]+"\nNombre de parties : "+nb_played[(int) PlayerType.MAGE];
        knightText.text = "Vague Record : "+best_waves[(int) PlayerType.KNIGHT]+"\nScore : "+best_scores[(int) PlayerType.KNIGHT]+"\nNombre de parties : "+nb_played[(int) PlayerType.KNIGHT];

    }
    
    private void Back()
    {
        Debug.Log ("Retour au menu");
        SceneManager.LoadScene(0);
    }

    private void SetBowman()
    {
        GameManager.instance.setPlayerType(PlayerType.BOWMAN);
        Debug.Log (PlayerType.BOWMAN + "sélectionné et lancement du jeu !");
        SceneManager.LoadScene(2);
    }

    private void SetMage()
    {
        GameManager.instance.setPlayerType(PlayerType.MAGE);
        Debug.Log (PlayerType.MAGE + "sélectionné et lancement du jeu !");
        SceneManager.LoadScene(2);

    }

    private void SetKnight()
    {
        GameManager.instance.setPlayerType(PlayerType.KNIGHT);
        Debug.Log (PlayerType.KNIGHT + "sélectionné et lancement du jeu !");
        SceneManager.LoadScene(2);


    }
}
