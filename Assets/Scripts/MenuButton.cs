﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MenuButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    [SerializeField] private GameObject partSystemMother;
    

    public void OnPointerEnter(PointerEventData eventData)
    {
        Debug.Log("playing");
        partSystemMother.SetActive(true);
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        Debug.Log("stopping");
        partSystemMother.SetActive(false);
    }
}
