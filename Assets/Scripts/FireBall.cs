﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : MonoBehaviour
{
    [SerializeField] private float init_speed = 20f;
    private AudioSource audioData;
    private Rigidbody rigi;
    private MageFactory factory;
    [SerializeField] private ParticleSystem boom;
    private float explForce = 3f;
    // Start is called before the first frame update
    void Awake()
    {
        rigi = GetComponent<Rigidbody>();
        audioData = GetComponent<AudioSource>();
    }
    
    public void Launch()
    {
        rigi.velocity = transform.forward * init_speed + Vector3.up * init_speed * 0.2f;
        audioData.Play(0);

    }

    private void OnCollisionEnter(Collision collision)
    {
        StartCoroutine(PlayParticle());
    }

    private void OnTriggerEnter(Collider other)
    {
        StartCoroutine(PlayParticle());
    }
    IEnumerator PlayParticle()
    {
        gameObject.GetComponent<Renderer>().enabled = false;
        gameObject.GetComponent<Collider>().enabled = false;
        
        Collider[] cols = Physics.OverlapSphere(transform.position, 3);
        foreach(Collider col in cols)
        {
            if(col.CompareTag("Ennemi") && col.isTrigger)
            {
                EnnemiCharacter script = col.gameObject.GetComponent<EnnemiCharacter>();
                int damage = 5;
                script.Hurt(damage);

                Vector3 force = col.transform.position - transform.position;
                force = force.normalized;
                script.AddForce(force * (damage * explForce));
            }
        }

        boom.Play();
        yield return new WaitForSeconds(boom.main.duration);
        gameObject.GetComponent<Renderer>().enabled = true;
        gameObject.GetComponent<Collider>().enabled = true;
        factory.Delete(gameObject, MageObject.FIREBALL);
    }

    public void Setfactory(MageFactory fact)
    {
        factory = fact;
    }
}