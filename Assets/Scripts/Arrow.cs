﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    [SerializeField] private float speed = 15f;
    [SerializeField] private ArcherObjectsFactory factory;

    private void Update()
    {
        transform.Translate(speed * Vector3.forward * Time.deltaTime);       
    }
    
    public void SetFactory(ArcherObjectsFactory fac)
    {
        factory = fac;
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("collision");
        if(collision.gameObject.CompareTag("Player"))
        {
            //Do nothing
        }
        else
        {
            //Ennemi or Wall
            factory.DeleteArrow(gameObject);
        }
    }
}
