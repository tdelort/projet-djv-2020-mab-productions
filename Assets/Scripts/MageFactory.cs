﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MageObject
{
    WALL,
    FIREBALL
}
public class MageFactory : MonoBehaviour
{
    [SerializeField] private GameObject wall_prefab;
    [SerializeField] private GameObject fireball_prefab;

    private Queue<GameObject> fireball_pool = new Queue<GameObject>();
    private Queue<GameObject> wall_pool = new Queue<GameObject>();

    private GameObject GetWall()
    {
        GameObject obj;
        if(wall_pool.Count != 0)
        {
            obj = wall_pool.Dequeue();
            obj.SetActive(true);
        }
        else
        {
            obj = Instantiate(wall_prefab);
        }
        return obj;
    }
    private GameObject GetFireball()
    {
        GameObject obj;
        if(fireball_pool.Count != 0)
        {
            obj = fireball_pool.Dequeue();
            obj.SetActive(true);
        }
        else
        {
            obj = Instantiate(fireball_prefab);
        }
        return obj;
    }

    public GameObject Get(MageObject type)
    {
        GameObject obj;
        switch (type)
        {
            case MageObject.WALL:
                obj = GetWall();
                break;
            case MageObject.FIREBALL:
                obj = GetFireball();
                break;
            default:
                obj = GetWall();
                break;
        }
        return obj;
    }

    public void Delete(GameObject obj, MageObject type)
    {
        obj.SetActive(false);
        switch (type)
        {
            case MageObject.WALL:
                wall_pool.Enqueue(obj);
                break;
            case MageObject.FIREBALL:
                fireball_pool.Enqueue(obj);
                break;
            default:
                break;
        }
    }
}
