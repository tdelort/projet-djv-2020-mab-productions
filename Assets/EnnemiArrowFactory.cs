﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemiArrowFactory : MonoBehaviour
{
    [SerializeField]
    private GameObject arrow_prefab;

    private Queue<GameObject> arrow_pool = new Queue<GameObject>();

    public GameObject GetArrow()
    {
        GameObject obj;
        if(arrow_pool.Count != 0)
        {
            obj = arrow_pool.Dequeue();
            obj.SetActive(true);
        }
        else
        {
            obj = Instantiate(arrow_prefab);
        }
        obj.tag = "EnnemiArrow";
        return obj;
    }

    public void DeleteArrow(GameObject arrow)
    {
        arrow.SetActive(false);
        arrow_pool.Enqueue(arrow);
    }
}
